eternity
========

A date/time detection and management module for the [Monkey programming language](https://github.com/blitz-research/monkey).
